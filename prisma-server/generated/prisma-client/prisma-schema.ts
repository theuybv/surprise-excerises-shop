export const typeDefs = /* GraphQL */ `type AggregateExercise {
  count: Int!
}

type AggregateLuckyCharm {
  count: Int!
}

type AggregatePlayer {
  count: Int!
}

type AggregateShop {
  count: Int!
}

type BatchPayload {
  count: Long!
}

type Exercise {
  id: ID!
  name: String!
  googleDriveVideoLink: String!
  videoLink: String
  thumbnailLink: String
  price: Int!
  embedSnippet: String!
  shop: Shop
  videoId: String
  thumbnailId: String
}

type ExerciseConnection {
  pageInfo: PageInfo!
  edges: [ExerciseEdge]!
  aggregate: AggregateExercise!
}

input ExerciseCreateInput {
  name: String!
  googleDriveVideoLink: String!
  videoLink: String
  thumbnailLink: String
  price: Int!
  embedSnippet: String!
  shop: ShopCreateOneWithoutExercisesInput
  videoId: String
  thumbnailId: String
}

input ExerciseCreateManyWithoutShopInput {
  create: [ExerciseCreateWithoutShopInput!]
  connect: [ExerciseWhereUniqueInput!]
}

input ExerciseCreateWithoutShopInput {
  name: String!
  googleDriveVideoLink: String!
  videoLink: String
  thumbnailLink: String
  price: Int!
  embedSnippet: String!
  videoId: String
  thumbnailId: String
}

type ExerciseEdge {
  node: Exercise!
  cursor: String!
}

enum ExerciseOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  googleDriveVideoLink_ASC
  googleDriveVideoLink_DESC
  videoLink_ASC
  videoLink_DESC
  thumbnailLink_ASC
  thumbnailLink_DESC
  price_ASC
  price_DESC
  embedSnippet_ASC
  embedSnippet_DESC
  videoId_ASC
  videoId_DESC
  thumbnailId_ASC
  thumbnailId_DESC
}

type ExercisePreviousValues {
  id: ID!
  name: String!
  googleDriveVideoLink: String!
  videoLink: String
  thumbnailLink: String
  price: Int!
  embedSnippet: String!
  videoId: String
  thumbnailId: String
}

input ExerciseScalarWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  googleDriveVideoLink: String
  googleDriveVideoLink_not: String
  googleDriveVideoLink_in: [String!]
  googleDriveVideoLink_not_in: [String!]
  googleDriveVideoLink_lt: String
  googleDriveVideoLink_lte: String
  googleDriveVideoLink_gt: String
  googleDriveVideoLink_gte: String
  googleDriveVideoLink_contains: String
  googleDriveVideoLink_not_contains: String
  googleDriveVideoLink_starts_with: String
  googleDriveVideoLink_not_starts_with: String
  googleDriveVideoLink_ends_with: String
  googleDriveVideoLink_not_ends_with: String
  videoLink: String
  videoLink_not: String
  videoLink_in: [String!]
  videoLink_not_in: [String!]
  videoLink_lt: String
  videoLink_lte: String
  videoLink_gt: String
  videoLink_gte: String
  videoLink_contains: String
  videoLink_not_contains: String
  videoLink_starts_with: String
  videoLink_not_starts_with: String
  videoLink_ends_with: String
  videoLink_not_ends_with: String
  thumbnailLink: String
  thumbnailLink_not: String
  thumbnailLink_in: [String!]
  thumbnailLink_not_in: [String!]
  thumbnailLink_lt: String
  thumbnailLink_lte: String
  thumbnailLink_gt: String
  thumbnailLink_gte: String
  thumbnailLink_contains: String
  thumbnailLink_not_contains: String
  thumbnailLink_starts_with: String
  thumbnailLink_not_starts_with: String
  thumbnailLink_ends_with: String
  thumbnailLink_not_ends_with: String
  price: Int
  price_not: Int
  price_in: [Int!]
  price_not_in: [Int!]
  price_lt: Int
  price_lte: Int
  price_gt: Int
  price_gte: Int
  embedSnippet: String
  embedSnippet_not: String
  embedSnippet_in: [String!]
  embedSnippet_not_in: [String!]
  embedSnippet_lt: String
  embedSnippet_lte: String
  embedSnippet_gt: String
  embedSnippet_gte: String
  embedSnippet_contains: String
  embedSnippet_not_contains: String
  embedSnippet_starts_with: String
  embedSnippet_not_starts_with: String
  embedSnippet_ends_with: String
  embedSnippet_not_ends_with: String
  videoId: String
  videoId_not: String
  videoId_in: [String!]
  videoId_not_in: [String!]
  videoId_lt: String
  videoId_lte: String
  videoId_gt: String
  videoId_gte: String
  videoId_contains: String
  videoId_not_contains: String
  videoId_starts_with: String
  videoId_not_starts_with: String
  videoId_ends_with: String
  videoId_not_ends_with: String
  thumbnailId: String
  thumbnailId_not: String
  thumbnailId_in: [String!]
  thumbnailId_not_in: [String!]
  thumbnailId_lt: String
  thumbnailId_lte: String
  thumbnailId_gt: String
  thumbnailId_gte: String
  thumbnailId_contains: String
  thumbnailId_not_contains: String
  thumbnailId_starts_with: String
  thumbnailId_not_starts_with: String
  thumbnailId_ends_with: String
  thumbnailId_not_ends_with: String
  AND: [ExerciseScalarWhereInput!]
  OR: [ExerciseScalarWhereInput!]
  NOT: [ExerciseScalarWhereInput!]
}

type ExerciseSubscriptionPayload {
  mutation: MutationType!
  node: Exercise
  updatedFields: [String!]
  previousValues: ExercisePreviousValues
}

input ExerciseSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: ExerciseWhereInput
  AND: [ExerciseSubscriptionWhereInput!]
  OR: [ExerciseSubscriptionWhereInput!]
  NOT: [ExerciseSubscriptionWhereInput!]
}

input ExerciseUpdateInput {
  name: String
  googleDriveVideoLink: String
  videoLink: String
  thumbnailLink: String
  price: Int
  embedSnippet: String
  shop: ShopUpdateOneWithoutExercisesInput
  videoId: String
  thumbnailId: String
}

input ExerciseUpdateManyDataInput {
  name: String
  googleDriveVideoLink: String
  videoLink: String
  thumbnailLink: String
  price: Int
  embedSnippet: String
  videoId: String
  thumbnailId: String
}

input ExerciseUpdateManyMutationInput {
  name: String
  googleDriveVideoLink: String
  videoLink: String
  thumbnailLink: String
  price: Int
  embedSnippet: String
  videoId: String
  thumbnailId: String
}

input ExerciseUpdateManyWithoutShopInput {
  create: [ExerciseCreateWithoutShopInput!]
  delete: [ExerciseWhereUniqueInput!]
  connect: [ExerciseWhereUniqueInput!]
  disconnect: [ExerciseWhereUniqueInput!]
  update: [ExerciseUpdateWithWhereUniqueWithoutShopInput!]
  upsert: [ExerciseUpsertWithWhereUniqueWithoutShopInput!]
  deleteMany: [ExerciseScalarWhereInput!]
  updateMany: [ExerciseUpdateManyWithWhereNestedInput!]
}

input ExerciseUpdateManyWithWhereNestedInput {
  where: ExerciseScalarWhereInput!
  data: ExerciseUpdateManyDataInput!
}

input ExerciseUpdateWithoutShopDataInput {
  name: String
  googleDriveVideoLink: String
  videoLink: String
  thumbnailLink: String
  price: Int
  embedSnippet: String
  videoId: String
  thumbnailId: String
}

input ExerciseUpdateWithWhereUniqueWithoutShopInput {
  where: ExerciseWhereUniqueInput!
  data: ExerciseUpdateWithoutShopDataInput!
}

input ExerciseUpsertWithWhereUniqueWithoutShopInput {
  where: ExerciseWhereUniqueInput!
  update: ExerciseUpdateWithoutShopDataInput!
  create: ExerciseCreateWithoutShopInput!
}

input ExerciseWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  googleDriveVideoLink: String
  googleDriveVideoLink_not: String
  googleDriveVideoLink_in: [String!]
  googleDriveVideoLink_not_in: [String!]
  googleDriveVideoLink_lt: String
  googleDriveVideoLink_lte: String
  googleDriveVideoLink_gt: String
  googleDriveVideoLink_gte: String
  googleDriveVideoLink_contains: String
  googleDriveVideoLink_not_contains: String
  googleDriveVideoLink_starts_with: String
  googleDriveVideoLink_not_starts_with: String
  googleDriveVideoLink_ends_with: String
  googleDriveVideoLink_not_ends_with: String
  videoLink: String
  videoLink_not: String
  videoLink_in: [String!]
  videoLink_not_in: [String!]
  videoLink_lt: String
  videoLink_lte: String
  videoLink_gt: String
  videoLink_gte: String
  videoLink_contains: String
  videoLink_not_contains: String
  videoLink_starts_with: String
  videoLink_not_starts_with: String
  videoLink_ends_with: String
  videoLink_not_ends_with: String
  thumbnailLink: String
  thumbnailLink_not: String
  thumbnailLink_in: [String!]
  thumbnailLink_not_in: [String!]
  thumbnailLink_lt: String
  thumbnailLink_lte: String
  thumbnailLink_gt: String
  thumbnailLink_gte: String
  thumbnailLink_contains: String
  thumbnailLink_not_contains: String
  thumbnailLink_starts_with: String
  thumbnailLink_not_starts_with: String
  thumbnailLink_ends_with: String
  thumbnailLink_not_ends_with: String
  price: Int
  price_not: Int
  price_in: [Int!]
  price_not_in: [Int!]
  price_lt: Int
  price_lte: Int
  price_gt: Int
  price_gte: Int
  embedSnippet: String
  embedSnippet_not: String
  embedSnippet_in: [String!]
  embedSnippet_not_in: [String!]
  embedSnippet_lt: String
  embedSnippet_lte: String
  embedSnippet_gt: String
  embedSnippet_gte: String
  embedSnippet_contains: String
  embedSnippet_not_contains: String
  embedSnippet_starts_with: String
  embedSnippet_not_starts_with: String
  embedSnippet_ends_with: String
  embedSnippet_not_ends_with: String
  videoId: String
  videoId_not: String
  videoId_in: [String!]
  videoId_not_in: [String!]
  videoId_lt: String
  videoId_lte: String
  videoId_gt: String
  videoId_gte: String
  videoId_contains: String
  videoId_not_contains: String
  videoId_starts_with: String
  videoId_not_starts_with: String
  videoId_ends_with: String
  videoId_not_ends_with: String
  thumbnailId: String
  thumbnailId_not: String
  thumbnailId_in: [String!]
  thumbnailId_not_in: [String!]
  thumbnailId_lt: String
  thumbnailId_lte: String
  thumbnailId_gt: String
  thumbnailId_gte: String
  thumbnailId_contains: String
  thumbnailId_not_contains: String
  thumbnailId_starts_with: String
  thumbnailId_not_starts_with: String
  thumbnailId_ends_with: String
  thumbnailId_not_ends_with: String
  AND: [ExerciseWhereInput!]
  OR: [ExerciseWhereInput!]
  NOT: [ExerciseWhereInput!]
}

input ExerciseWhereUniqueInput {
  id: ID
}

scalar Long

type LuckyCharm {
  id: ID!
  workoutType: WORKOUT_TYPES!
  operation: OPERATION_TYPES!
  value: Int!
  price: Int!
}

type LuckyCharmConnection {
  pageInfo: PageInfo!
  edges: [LuckyCharmEdge]!
  aggregate: AggregateLuckyCharm!
}

input LuckyCharmCreateInput {
  workoutType: WORKOUT_TYPES!
  operation: OPERATION_TYPES!
  value: Int!
  price: Int!
}

type LuckyCharmEdge {
  node: LuckyCharm!
  cursor: String!
}

enum LuckyCharmOrderByInput {
  id_ASC
  id_DESC
  workoutType_ASC
  workoutType_DESC
  operation_ASC
  operation_DESC
  value_ASC
  value_DESC
  price_ASC
  price_DESC
}

type LuckyCharmPreviousValues {
  id: ID!
  workoutType: WORKOUT_TYPES!
  operation: OPERATION_TYPES!
  value: Int!
  price: Int!
}

type LuckyCharmSubscriptionPayload {
  mutation: MutationType!
  node: LuckyCharm
  updatedFields: [String!]
  previousValues: LuckyCharmPreviousValues
}

input LuckyCharmSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: LuckyCharmWhereInput
  AND: [LuckyCharmSubscriptionWhereInput!]
  OR: [LuckyCharmSubscriptionWhereInput!]
  NOT: [LuckyCharmSubscriptionWhereInput!]
}

input LuckyCharmUpdateInput {
  workoutType: WORKOUT_TYPES
  operation: OPERATION_TYPES
  value: Int
  price: Int
}

input LuckyCharmUpdateManyMutationInput {
  workoutType: WORKOUT_TYPES
  operation: OPERATION_TYPES
  value: Int
  price: Int
}

input LuckyCharmWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  workoutType: WORKOUT_TYPES
  workoutType_not: WORKOUT_TYPES
  workoutType_in: [WORKOUT_TYPES!]
  workoutType_not_in: [WORKOUT_TYPES!]
  operation: OPERATION_TYPES
  operation_not: OPERATION_TYPES
  operation_in: [OPERATION_TYPES!]
  operation_not_in: [OPERATION_TYPES!]
  value: Int
  value_not: Int
  value_in: [Int!]
  value_not_in: [Int!]
  value_lt: Int
  value_lte: Int
  value_gt: Int
  value_gte: Int
  price: Int
  price_not: Int
  price_in: [Int!]
  price_not_in: [Int!]
  price_lt: Int
  price_lte: Int
  price_gt: Int
  price_gte: Int
  AND: [LuckyCharmWhereInput!]
  OR: [LuckyCharmWhereInput!]
  NOT: [LuckyCharmWhereInput!]
}

input LuckyCharmWhereUniqueInput {
  id: ID
}

type Mutation {
  createExercise(data: ExerciseCreateInput!): Exercise!
  updateExercise(data: ExerciseUpdateInput!, where: ExerciseWhereUniqueInput!): Exercise
  updateManyExercises(data: ExerciseUpdateManyMutationInput!, where: ExerciseWhereInput): BatchPayload!
  upsertExercise(where: ExerciseWhereUniqueInput!, create: ExerciseCreateInput!, update: ExerciseUpdateInput!): Exercise!
  deleteExercise(where: ExerciseWhereUniqueInput!): Exercise
  deleteManyExercises(where: ExerciseWhereInput): BatchPayload!
  createLuckyCharm(data: LuckyCharmCreateInput!): LuckyCharm!
  updateLuckyCharm(data: LuckyCharmUpdateInput!, where: LuckyCharmWhereUniqueInput!): LuckyCharm
  updateManyLuckyCharms(data: LuckyCharmUpdateManyMutationInput!, where: LuckyCharmWhereInput): BatchPayload!
  upsertLuckyCharm(where: LuckyCharmWhereUniqueInput!, create: LuckyCharmCreateInput!, update: LuckyCharmUpdateInput!): LuckyCharm!
  deleteLuckyCharm(where: LuckyCharmWhereUniqueInput!): LuckyCharm
  deleteManyLuckyCharms(where: LuckyCharmWhereInput): BatchPayload!
  createPlayer(data: PlayerCreateInput!): Player!
  updatePlayer(data: PlayerUpdateInput!, where: PlayerWhereUniqueInput!): Player
  updateManyPlayers(data: PlayerUpdateManyMutationInput!, where: PlayerWhereInput): BatchPayload!
  upsertPlayer(where: PlayerWhereUniqueInput!, create: PlayerCreateInput!, update: PlayerUpdateInput!): Player!
  deletePlayer(where: PlayerWhereUniqueInput!): Player
  deleteManyPlayers(where: PlayerWhereInput): BatchPayload!
  createShop(data: ShopCreateInput!): Shop!
  updateShop(data: ShopUpdateInput!, where: ShopWhereUniqueInput!): Shop
  updateManyShops(data: ShopUpdateManyMutationInput!, where: ShopWhereInput): BatchPayload!
  upsertShop(where: ShopWhereUniqueInput!, create: ShopCreateInput!, update: ShopUpdateInput!): Shop!
  deleteShop(where: ShopWhereUniqueInput!): Shop
  deleteManyShops(where: ShopWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

interface Node {
  id: ID!
}

enum OPERATION_TYPES {
  SUBTRACT
}

type PageInfo {
  hasNextPage: Boolean!
  hasPreviousPage: Boolean!
  startCursor: String
  endCursor: String
}

type Player {
  id: ID!
  name: String!
  shoppingCode: String!
}

type PlayerConnection {
  pageInfo: PageInfo!
  edges: [PlayerEdge]!
  aggregate: AggregatePlayer!
}

input PlayerCreateInput {
  name: String!
  shoppingCode: String!
}

type PlayerEdge {
  node: Player!
  cursor: String!
}

enum PlayerOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  shoppingCode_ASC
  shoppingCode_DESC
}

type PlayerPreviousValues {
  id: ID!
  name: String!
  shoppingCode: String!
}

type PlayerSubscriptionPayload {
  mutation: MutationType!
  node: Player
  updatedFields: [String!]
  previousValues: PlayerPreviousValues
}

input PlayerSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: PlayerWhereInput
  AND: [PlayerSubscriptionWhereInput!]
  OR: [PlayerSubscriptionWhereInput!]
  NOT: [PlayerSubscriptionWhereInput!]
}

input PlayerUpdateInput {
  name: String
  shoppingCode: String
}

input PlayerUpdateManyMutationInput {
  name: String
  shoppingCode: String
}

input PlayerWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  shoppingCode: String
  shoppingCode_not: String
  shoppingCode_in: [String!]
  shoppingCode_not_in: [String!]
  shoppingCode_lt: String
  shoppingCode_lte: String
  shoppingCode_gt: String
  shoppingCode_gte: String
  shoppingCode_contains: String
  shoppingCode_not_contains: String
  shoppingCode_starts_with: String
  shoppingCode_not_starts_with: String
  shoppingCode_ends_with: String
  shoppingCode_not_ends_with: String
  AND: [PlayerWhereInput!]
  OR: [PlayerWhereInput!]
  NOT: [PlayerWhereInput!]
}

input PlayerWhereUniqueInput {
  id: ID
}

type Query {
  exercise(where: ExerciseWhereUniqueInput!): Exercise
  exercises(where: ExerciseWhereInput, orderBy: ExerciseOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Exercise]!
  exercisesConnection(where: ExerciseWhereInput, orderBy: ExerciseOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): ExerciseConnection!
  luckyCharm(where: LuckyCharmWhereUniqueInput!): LuckyCharm
  luckyCharms(where: LuckyCharmWhereInput, orderBy: LuckyCharmOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [LuckyCharm]!
  luckyCharmsConnection(where: LuckyCharmWhereInput, orderBy: LuckyCharmOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): LuckyCharmConnection!
  player(where: PlayerWhereUniqueInput!): Player
  players(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Player]!
  playersConnection(where: PlayerWhereInput, orderBy: PlayerOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): PlayerConnection!
  shop(where: ShopWhereUniqueInput!): Shop
  shops(where: ShopWhereInput, orderBy: ShopOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Shop]!
  shopsConnection(where: ShopWhereInput, orderBy: ShopOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): ShopConnection!
  node(id: ID!): Node
}

type Shop {
  id: ID!
  name: String!
  exercises(where: ExerciseWhereInput, orderBy: ExerciseOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Exercise!]
  shoppingCode: String!
}

type ShopConnection {
  pageInfo: PageInfo!
  edges: [ShopEdge]!
  aggregate: AggregateShop!
}

input ShopCreateInput {
  name: String!
  exercises: ExerciseCreateManyWithoutShopInput
  shoppingCode: String!
}

input ShopCreateOneWithoutExercisesInput {
  create: ShopCreateWithoutExercisesInput
  connect: ShopWhereUniqueInput
}

input ShopCreateWithoutExercisesInput {
  name: String!
  shoppingCode: String!
}

type ShopEdge {
  node: Shop!
  cursor: String!
}

enum ShopOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  shoppingCode_ASC
  shoppingCode_DESC
}

type ShopPreviousValues {
  id: ID!
  name: String!
  shoppingCode: String!
}

type ShopSubscriptionPayload {
  mutation: MutationType!
  node: Shop
  updatedFields: [String!]
  previousValues: ShopPreviousValues
}

input ShopSubscriptionWhereInput {
  mutation_in: [MutationType!]
  updatedFields_contains: String
  updatedFields_contains_every: [String!]
  updatedFields_contains_some: [String!]
  node: ShopWhereInput
  AND: [ShopSubscriptionWhereInput!]
  OR: [ShopSubscriptionWhereInput!]
  NOT: [ShopSubscriptionWhereInput!]
}

input ShopUpdateInput {
  name: String
  exercises: ExerciseUpdateManyWithoutShopInput
  shoppingCode: String
}

input ShopUpdateManyMutationInput {
  name: String
  shoppingCode: String
}

input ShopUpdateOneWithoutExercisesInput {
  create: ShopCreateWithoutExercisesInput
  update: ShopUpdateWithoutExercisesDataInput
  upsert: ShopUpsertWithoutExercisesInput
  delete: Boolean
  disconnect: Boolean
  connect: ShopWhereUniqueInput
}

input ShopUpdateWithoutExercisesDataInput {
  name: String
  shoppingCode: String
}

input ShopUpsertWithoutExercisesInput {
  update: ShopUpdateWithoutExercisesDataInput!
  create: ShopCreateWithoutExercisesInput!
}

input ShopWhereInput {
  id: ID
  id_not: ID
  id_in: [ID!]
  id_not_in: [ID!]
  id_lt: ID
  id_lte: ID
  id_gt: ID
  id_gte: ID
  id_contains: ID
  id_not_contains: ID
  id_starts_with: ID
  id_not_starts_with: ID
  id_ends_with: ID
  id_not_ends_with: ID
  name: String
  name_not: String
  name_in: [String!]
  name_not_in: [String!]
  name_lt: String
  name_lte: String
  name_gt: String
  name_gte: String
  name_contains: String
  name_not_contains: String
  name_starts_with: String
  name_not_starts_with: String
  name_ends_with: String
  name_not_ends_with: String
  shoppingCode: String
  shoppingCode_not: String
  shoppingCode_in: [String!]
  shoppingCode_not_in: [String!]
  shoppingCode_lt: String
  shoppingCode_lte: String
  shoppingCode_gt: String
  shoppingCode_gte: String
  shoppingCode_contains: String
  shoppingCode_not_contains: String
  shoppingCode_starts_with: String
  shoppingCode_not_starts_with: String
  shoppingCode_ends_with: String
  shoppingCode_not_ends_with: String
  AND: [ShopWhereInput!]
  OR: [ShopWhereInput!]
  NOT: [ShopWhereInput!]
}

input ShopWhereUniqueInput {
  id: ID
}

type Subscription {
  exercise(where: ExerciseSubscriptionWhereInput): ExerciseSubscriptionPayload
  luckyCharm(where: LuckyCharmSubscriptionWhereInput): LuckyCharmSubscriptionPayload
  player(where: PlayerSubscriptionWhereInput): PlayerSubscriptionPayload
  shop(where: ShopSubscriptionWhereInput): ShopSubscriptionPayload
}

enum WORKOUT_TYPES {
  REPETITION
  TIME
}
`