// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { QueryResolvers } from "../graphqlgen";

export const Query: QueryResolvers.Type = {
  ...QueryResolvers.defaultResolvers,
  exercises: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  players: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  shops: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  luckyCharms: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
