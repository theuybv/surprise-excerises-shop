// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { ShopResolvers } from "../graphqlgen";

export const Shop: ShopResolvers.Type = {
  ...ShopResolvers.defaultResolvers,

  exercises: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
