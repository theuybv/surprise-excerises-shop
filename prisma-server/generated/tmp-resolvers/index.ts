// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { Resolvers } from "../graphqlgen";

import { Query } from "./Query";
import { Exercise } from "./Exercise";
import { Shop } from "./Shop";
import { Player } from "./Player";
import { LuckyCharm } from "./LuckyCharm";

export const resolvers: Resolvers = {
  Query,
  Exercise,
  Shop,
  Player,
  LuckyCharm
};
