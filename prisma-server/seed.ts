import {Exercise, LuckyCharm, Player, Shop, ShopCreateInput} from "./generated/prisma-client";
import * as _ from 'lodash';
import {Db, GridFSBucket, MongoClient} from "mongodb";
import {connectedMongoClient} from "./mongoClient";

const {prisma} = require('./generated/prisma-client');
const exercisesWithPriceList = require('./exercisesWithPriceList.json');
const luckyCharms = require('./luckyCharms.json');
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const mongodb = require('mongodb');


const main = async () => {
    const client: MongoClient = await connectedMongoClient;
    const db: Db = client.db('default_default');
    const bucket: GridFSBucket = new mongodb.GridFSBucket(db);

    try {
        await Promise.all([
            db.dropCollection('fs.chunks'),
            db.dropCollection('fs.files')
        ]);
    } catch (e) {
        console.log(e);
    }
    await Promise.all(luckyCharms.map(async (luckyCharm: LuckyCharm) => {
        return await prisma.createLuckyCharm(luckyCharm);
    }));

    const SHOP_IMMORTAL = await prisma.createShop({
        name: "Immortal",
        shoppingCode: 'tE62'
    } as Shop);
    const SHOP_TO_THE_LIMIT = await prisma.createShop({
        name: "To The Limit",
        shoppingCode: '05DQ'
    } as Shop);
    const SHOP_NO_ROOM_FOR_ESCAPE = await prisma.createShop({
        name: "No room for escape",
        shoppingCode: '4i9r'
    } as Shop);

    const shops: Shop[] = [SHOP_IMMORTAL, SHOP_TO_THE_LIMIT, SHOP_NO_ROOM_FOR_ESCAPE];

    await Promise.all([
        prisma.createPlayer({
            name: "Tjerk Lugthart",
            shoppingCode: SHOP_IMMORTAL.shoppingCode
        } as Player),
        prisma.createPlayer({
            name: "Theuy Limpanont",
            shoppingCode: SHOP_TO_THE_LIMIT.shoppingCode
        } as Player),
        prisma.createPlayer({
            name: "Pieter Bas Lugthart",
            shoppingCode: SHOP_NO_ROOM_FOR_ESCAPE.shoppingCode
        } as Player),
    ]);


    await Promise.all(exercisesWithPriceList.map(async ({id, name, videoLink, embedSnippet, price}: Exercise) => {
        const googleDriveVideoLink = videoLink;
        const videoFileName = _.kebabCase(name) + '.mp4';
        const thumbnailFileName = _.kebabCase(name) + '.jpg';
        const videoId = _.uniqueId(videoFileName + '_');
        const thumbnailId = _.uniqueId(thumbnailFileName + '_');

        return new Promise(async (resolve, reject) => {
            fs.createReadStream(path.resolve(__dirname, `../videos/${videoFileName}`))
                .pipe(bucket.openUploadStreamWithId(videoId, videoFileName))
                .on('error', function (error) {
                    assert.ifError(error);
                    reject(error);
                })
                .on('finish', function () {
                    console.log('done!');
                    fs.createReadStream(path.resolve(__dirname, `../videos/thumbs/${thumbnailFileName}`))
                        .pipe(bucket.openUploadStreamWithId(thumbnailId, thumbnailFileName))
                        .on('error', function (error) {
                            assert.ifError(error);
                            reject(error);
                        })
                        .on('finish', async function () {
                            console.log('done!');
                            const exercise = await prisma.createExercise({
                                name,
                                googleDriveVideoLink,
                                embedSnippet,
                                price: price * parseInt(_.sample([1.8, 2]) as any),
                                videoId,
                                thumbnailId
                            } as Exercise);
                            await prisma.updateShop({
                                where: {id: (_.sample(shops) as any).id}, data: {
                                    exercises: {
                                        connect: {
                                            id: exercise.id
                                        }
                                    }
                                }
                            });
                            resolve(exercise);
                        });
                });
        });
    }));

    process.exit(0);

};

main().catch(console.error);