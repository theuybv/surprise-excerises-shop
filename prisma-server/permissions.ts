import {and, rule, shield} from 'graphql-shield'
import {Context} from "./types";

const rules = {
    playerHasPermissionToShop: rule({ cache: 'no_cache' })(async (parent, {shoppingCode}, context: Context) => {
        return true;
    }),
};

export const permissions = shield({
    Shop: {
        exercises: and(rules.playerHasPermissionToShop)
    }
});