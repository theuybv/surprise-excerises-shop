import {MongoClient} from "mongodb";

const mongodb = require('mongodb');

const uri = 'mongodb://prisma:prisma@localhost:27017?authMechanism=DEFAULT&authSource=admin';

export const connectedMongoClient: Promise<MongoClient> = mongodb.MongoClient.connect(uri, {useNewUrlParser: true});

