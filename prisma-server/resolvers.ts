import {Context} from "./types";
import {
    ExerciseResolvers, LuckyCharmResolvers,
    PlayerResolvers,
    QueryResolvers,
    ShopResolvers,
} from "./generated/graphqlgen";
import ArgsExercises = QueryResolvers.ArgsExercises;
import {
    Exercise,
    ExerciseWhereInput,
    LuckyCharmWhereInput,
    PlayerWhereInput,
    ShopWhereInput
} from "./generated/prisma-client";
import ArgsPlayers = QueryResolvers.ArgsPlayers;
import ArgsShops = QueryResolvers.ArgsShops;
import ArgsLuckyCharms = QueryResolvers.ArgsLuckyCharms;
import * as _ from 'lodash';

const Query: QueryResolvers.Type = {
    ...QueryResolvers.defaultResolvers,
    exercises(parent, args: ArgsExercises, context: Context) {
        return context.db.exercises({where: args.where as ExerciseWhereInput});
    },
    players(parent, args: ArgsPlayers, context: Context) {
        return context.db.players({where: args.where as PlayerWhereInput})
    },
    shops(parent, args: ArgsShops, context: Context) {
        return context.db.shops({where: args.where as ShopWhereInput})
    },
    luckyCharms(parent, args: ArgsLuckyCharms, context: Context) {
        return context.db.luckyCharms({where: args.where as LuckyCharmWhereInput})
    },
};

const Player: PlayerResolvers.Type = {
    ...PlayerResolvers.defaultResolvers
};

const Exercise: ExerciseResolvers.Type = {
    ...ExerciseResolvers.defaultResolvers,
    videoLink: (parent: Exercise) => {
        return '/files/' +  _.kebabCase(parent.name) + '.mp4';
    },
    thumbnailLink: (parent: Exercise) => {
        return '/files/' +  _.kebabCase(parent.name) + '.jpg';
    },
    shop: (parent, args, ctx) => {
        return ctx.db.exercise({id: parent.id}).shop();
    }
};

const Shop: ShopResolvers.Type = {
    ...ShopResolvers.defaultResolvers,

    exercises: (parent, args, ctx) => {
        return ctx.db.shop({id: parent.id}).exercises();
    }
};

const LuckyCharm: LuckyCharmResolvers.Type = {
    ...LuckyCharmResolvers.defaultResolvers
};

export const resolvers: any = {
    Query,
    Exercise,
    Shop,
    Player,
    LuckyCharm
};