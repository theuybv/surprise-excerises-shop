import { Prisma } from './generated/prisma-client'
import {Request} from "express-serve-static-core";

export interface Context {
    request: Request,
    db: Prisma
}