import {prisma} from './generated/prisma-client'
import {GraphQLServer} from 'graphql-yoga'
import {resolvers} from "./resolvers";
import {Request, Response} from "express-serve-static-core";
import {Db, GridFSBucket, MongoClient} from "mongodb";
import {connectedMongoClient} from "./mongoClient";
import mongodb from 'mongodb';
import * as fs from "fs";
import {Context} from "./types";
import {permissions} from "./permissions";

const server = new GraphQLServer({
    typeDefs: './schema.graphql',
    resolvers,
    middlewares: [permissions],
    context: (request: Request): Context => {
        return {
            request,
            db: prisma
        }
    },
});


server.express.get('/files/:fileName', async (req: Request, res: Response) => {
    const client: MongoClient = await connectedMongoClient;
    const db: Db = client.db('default_default');
    const bucket: GridFSBucket = new mongodb.GridFSBucket(db, {
        bucketName: 'fs'
    });
    bucket.openDownloadStreamByName(req.params.fileName).pipe(res);
});
server.start({endpoint: process.env.GRAPHQL_ENDPOINT || ''},() => console.log('Server is running on http://localhost:4000' ));