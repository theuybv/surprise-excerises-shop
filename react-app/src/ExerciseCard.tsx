import {Theme, withStyles, WithStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import * as React from "react";
import {CardContent, Icon} from "@material-ui/core";
import {Exercise} from "../../prisma-server/generated/prisma-client";
import {gql} from "apollo-boost";
import Mutation from "react-apollo/Mutation";
import {Query} from "react-apollo";


const styles = (theme: Theme) => ({
    card: {
        maxWidth: 345,
    },
    media: {
        height: 240,
        // width: "100%"
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});

interface Props extends WithStyles<typeof styles> {
    exercise: Exercise
    videoSrcTag: JSX.Element
}

const ADD_EXCERISE_TO_SHOPPING_CART = gql`
    mutation addExerciseToShoppingCart($exerciseId: String!) {
        addExerciseToShoppingCart(exerciseId: $exerciseId) @client
    }
`;

export const GET_SHOPPING_CODE = gql`
    {
        shoppingCode @client
    }
`;
const MediaCard = (props: Props) => {
    const {classes, exercise, videoSrcTag} = props;
    return (
        <Mutation mutation={ADD_EXCERISE_TO_SHOPPING_CART} variables={{exerciseId: exercise.id}}>
            {addExerciseToShoppingCart => (
                <Card className={classes.card}>
                    <CardContent>
                        {videoSrcTag}
                        <Typography gutterBottom variant="h6" component="h2" color={"primary"}>
                            {exercise.name}
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Typography component="p" variant="h6" style={{flexGrow: 1}}>
                            {exercise.price},-
                        </Typography>
                        <Query query={GET_SHOPPING_CODE}>
                            {({loading, error, data}) => {
                                if (loading) return 'Loading...';
                                if (error) return `Error! ${error.message}`;
                                return (
                                    (exercise['shop'].shoppingCode === data.shoppingCode)
                                        ?
                                        <Button variant="contained" color="primary"
                                                onClick={() => addExerciseToShoppingCart()}>
                                            <Icon className={classes.rightIcon}>shopping_cart</Icon>
                                        </Button>
                                        : null
                                )
                            }}
                        </Query>
                    </CardActions>
                </Card>
            )}
        </Mutation>
    );
};


export default withStyles(styles, {withTheme: true})(MediaCard);
