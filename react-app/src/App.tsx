import React, {Component} from 'react';
import {ExercisesList} from "./ExercisesList";
import AppContainer from "./AppContainer";
import {Grid} from '@material-ui/core';
import {HashRouter as Router, Route, Link, RouteComponentProps, Redirect} from "react-router-dom";
import {LuckyCharmsList} from "./LuckyCharmsList";

class App extends Component {
    render() {
        return (
            <Router>
                <AppContainer>
                    <Grid container style={{paddingTop: 75}}>
                        <Grid item xs={12}>
                            <Route path="/shops/:shopId" exact render={(props: RouteComponentProps) => {
                                return (
                                    <ExercisesList shopId={props.match.params['shopId']}/>
                                )
                            }}/>

                            <Route path="/lucky-charms" exact render={(props: RouteComponentProps) => {
                                return (
                                   <LuckyCharmsList />
                                )
                            }}/>
                        </Grid>
                    </Grid>
                </AppContainer>
            </Router>
        );
    }
}

export default App;
