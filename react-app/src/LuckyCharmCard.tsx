import {Theme, withStyles, WithStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import * as React from "react";
import {CardContent, Icon} from "@material-ui/core";
import {Exercise, LuckyCharm, OPERATION_TYPES, WORKOUT_TYPES} from "../../prisma-server/generated/prisma-client";
import {gql} from "apollo-boost";
import Mutation from "react-apollo/Mutation";


const styles = (theme: Theme) => ({
    card: {
        maxWidth: 345,
        width: 345,
    },
    media: {
        height: 240,
        // width: "100%"
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});

interface Props extends WithStyles<typeof styles> {
    luckyCharm: LuckyCharm
}

const ADD_LUCKY_CHARM_TO_SHOPPING_CART = gql`
    mutation addLuckyCharmToShoppingCart($luckyCharmId: String!) {
        addLuckyCharmToShoppingCart(luckyCharmId: $luckyCharmId) @client
    }
`;


const MediaCard = (props: Props) => {
    const {classes, luckyCharm} = props;
    return (
        <Mutation mutation={ADD_LUCKY_CHARM_TO_SHOPPING_CART} variables={{luckyCharmId: luckyCharm.id}}>
            {addLuckyCharmToShoppingCart => (
                <Card className={classes.card}>
                    <CardContent>
                        <Typography gutterBottom variant="h6" component="h2" color={"primary"}>
                            {((luckyCharm.workoutType === 'REPETITION' as WORKOUT_TYPES) ? 'Herhalingen' : 'Tijd')}
                        </Typography>
                        <Typography gutterBottom variant="body1" component="p" color={"primary"}>
                            {(luckyCharm.workoutType === 'REPETITION' as WORKOUT_TYPES) ?
                                 luckyCharm.value + ' sec. minder' + ' oefening uitvoeren'
                                :
                                luckyCharm.value + ' herhalingen verminderen'
                            }
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Typography component="p" variant="h6" style={{flexGrow: 1}}>
                            {luckyCharm.price},-
                        </Typography>
                        <Button variant="contained" color="primary"
                                onClick={() => addLuckyCharmToShoppingCart()}>
                            <Icon className={classes.rightIcon}>shopping_cart</Icon>
                        </Button>
                    </CardActions>
                </Card>
            )}
        </Mutation>
    );
};


export default withStyles(styles, {withTheme: true})(MediaCard);
