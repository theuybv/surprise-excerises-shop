import {Theme, WithStyles, withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import * as React from "react";
import {CardContent, CardHeader, Grid} from "@material-ui/core";
import {Exercise} from "../../prisma-server/generated/prisma-client";
import {gql} from "apollo-boost";
import Mutation from "react-apollo/Mutation";
import {map, chain} from 'lodash';

const styles = (theme: Theme) => ({
    card: {
        maxWidth: 600,
        width: 400
    },
    media: {
        height: 240,
        // width: "100%"
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});

const REMOVE_PRODUCTS_FROM_CART_MUTATION = gql`
    mutation removeProductsFromShoppingCart($productId: String!) {
        removeProductsFromShoppingCart(productId: $productId)  @client
    }
`;
export interface IShoppingCartItem {
    id: string, product: {id: string, name: string, price: number, image?: string}
}
interface Props extends WithStyles<typeof styles> {
    items: IShoppingCartItem[],
}

function MediaCard(props: Props) {
    const {classes, items} = props;
    return (
        <Card className={classes.card}>
            {(items.length > 0) ?
                <React.Fragment>
                    <CardContent>
                        <Grid container>
                            <Grid item xs={12}>
                                {map(chain(items).groupBy('product.id').value(), (groupItems: IShoppingCartItem[], productId: string) => {
                                    return (
                                        <Mutation mutation={REMOVE_PRODUCTS_FROM_CART_MUTATION} variables={{productId}}
                                                  key={Math.random() * 100000}>
                                            {removeProductsFromShoppingCart => (
                                                <Grid container>
                                                    <Grid item xs={9}>
                                                        <Typography variant="body1" component="p" color={"primary"}>
                                                            {(groupItems.length > 1)
                                                                ? groupItems.length + ' x -' + groupItems[0].product.name
                                                                : groupItems[0].product.name
                                                            }
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item xs={3} style={{display: 'inline-flex'}}>
                                                        <DeleteIcon color={"inherit"}
                                                                    style={{flexGrow: 1, cursor: "pointer"}}
                                                                    onClick={() => removeProductsFromShoppingCart() as any}
                                                        />
                                                        <Typography variant="body1" component="p">
                                                            € {groupItems[0].product.price * groupItems.length}
                                                        </Typography>
                                                    </Grid>
                                                </Grid>
                                            )}
                                        </Mutation>
                                    )
                                })}
                                <hr/>
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Typography variant="body1" component="p"
                                                style={{flexGrow: 1, textAlign: 'right', marginRight: 10}}>
                                        <strong>Totaal bedrag</strong>
                                    </Typography>
                                    <Typography variant="body1" component="p">
                                        <strong>€ {items.reduce((acc: number, cur: IShoppingCartItem) => {
                                            return acc + cur.product.price;
                                        }, 0)}</strong>
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </CardContent>
                    {/*<Grid container justify={"flex-end"}>
                        <CardActions>
                            <Button variant={"text"} color={"default"}>Wijzig winkelmand</Button>
                            <Button variant="contained" color="secondary">
                                Bestellen
                            </Button>
                        </CardActions>
                    </Grid>*/}
                </React.Fragment>
                :
                <React.Fragment>
                    <CardHeader title={"Geen oefeningen gekozen"}/>
                </React.Fragment>
            }
        </Card>
    );
}


export default withStyles(styles, {withTheme: true})(MediaCard);
