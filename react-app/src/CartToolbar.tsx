import {Badge, IconButton, Popover, withStyles} from "@material-ui/core";
import React from "react";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import {compose, graphql, Query} from "react-apollo";
import {gql} from "apollo-boost";
import CartPopoverContent from "./CartPopoverContent";
import {CartSwipeableDrawer, CLOSE_CART_DRAWER, OPEN_CART_DRAWER} from "./CartSwipeableDrawer";


const SHOPPING_CART_ITEMS = gql`
    {
        cartDrawerOpen @client
        shoppingCartItems  @client {
            id
            product {
                id
                name
                price
                image
            }
        }
        }
`;

class CartToolbar extends React.Component<{
    openCartDrawer: Function
    closeCartDrawer: Function
}, any> {

    state = {
        anchorEl: null,
    };

    render() {
        const {anchorEl} = this.state;
        const popOverOpen = Boolean(anchorEl);
        const {openCartDrawer, closeCartDrawer} = this.props;
        return (
            <Query query={SHOPPING_CART_ITEMS}>
                {({data, client}) => (
                    <div>
                        <CartSwipeableDrawer items={data.shoppingCartItems} open={data.cartDrawerOpen}/>
                        <IconButton
                            onClick={(event: any) => {
                                openCartDrawer()
                            }}
                            color="inherit"
                        >
                            <Badge badgeContent={data.shoppingCartItems.length} color="secondary">
                                <ShoppingCartIcon/>
                            </Badge>
                        </IconButton>
                        <Popover
                            open={popOverOpen}
                            anchorEl={anchorEl}
                            onClose={() => {
                                this.setState({
                                    anchorEl: null,
                                });
                            }}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                        >
                            <CartPopoverContent items={data.shoppingCartItems}/>
                        </Popover>
                    </div>
                )}
            </Query>
        )
    }
}

export default compose(
    withStyles({}, {withTheme: true}),
    graphql(CLOSE_CART_DRAWER, {name: 'closeCartDrawer'}),
    graphql(OPEN_CART_DRAWER, {name: 'openCartDrawer'})
)(CartToolbar);