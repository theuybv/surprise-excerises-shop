
// @ts-ignore
const proxy = require('http-proxy-middleware');

module.exports = function (app) {
    app.use(proxy('/files', {target: 'http://localhost:4000/'}))
};