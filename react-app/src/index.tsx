import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {ApolloClient, HttpLink, InMemoryCache, ApolloLink, gql} from 'apollo-boost';
import {ApolloProvider} from "react-apollo";
import {withClientState} from 'apollo-link-state';
import {GET_EXERCISES} from "./ExercisesList";
import {Exercise, LuckyCharm, WORKOUT_TYPES} from "../../prisma-server/generated/prisma-client";
import {kebabCase, uniqueId} from 'lodash';
import {IShoppingCartItem} from "./CartPopoverContent";
import {ALL_LUCKY_CHARMS} from "./LuckyCharmsList";
import {persistCache} from 'apollo-cache-persist';

const GET_SHOPPING_CART_ITEMS_QUERY = gql`
    {
        shoppingCartItems @client {
            id
            product {
                id
                name
                price
                image
            }
        }
    }
`;

const cache = new InMemoryCache();

persistCache({
    cache: cache,
    storage: window.localStorage as any
}).then(() => {

}).catch((e: any) => {
    console.error(e);
});

const stateLink = withClientState({
    cache,
    defaults: {
        addToShoppingCartItemId: null,
        cartDrawerOpen: false,
        shoppingCode: '',
        shoppingCartItems: [],
        networkStatus: {
            __typename: 'NetworkStatus',
            isConnected: false,
        }
    },
    resolvers: {
        Mutation: {
            setShoppingCode: (_, variables, {cache, getCacheKey}) => {
                const data = {shoppingCode: variables.shoppingCode};
                cache.writeData({data});
                return data;
            },
            closeCartDrawer: (_, variables, {cache, getCacheKey}) => {
                const data = {cartDrawerOpen: false};
                cache.writeData({data});
                return data;
            },
            openCartDrawer: (_, variables, {cache, getCacheKey}) => {
                const data = {cartDrawerOpen: true};
                cache.writeData({data});
                return data;
            },
            removeProductsFromShoppingCart: (_, variables, {cache, getCacheKey}) => {
                const previous = cache.readQuery({query: GET_SHOPPING_CART_ITEMS_QUERY});
                const data = {
                    shoppingCartItems: previous.shoppingCartItems.filter((item: IShoppingCartItem) => {
                        return item.product.id !== variables.productId;
                    })
                };
                cache.writeData({data});
                return data;
            },
            removeItemFromShoppingCart: (_, variables, {cache, getCacheKey}) => {
                const previous = cache.readQuery({query: GET_SHOPPING_CART_ITEMS_QUERY});
                const data = {
                    shoppingCartItems: previous.shoppingCartItems.filter((item: IShoppingCartItem) => {
                        return item.id !== variables.shoppingCartItemId;
                    })
                };
                cache.writeData({data});
                return data;
            },
            addExerciseToShoppingCart: (_, variables, {cache, getCacheKey}) => {
                const previous = cache.readQuery({query: GET_SHOPPING_CART_ITEMS_QUERY});
                const _data: { exercises: Exercise[] } = cache.readQuery({query: GET_EXERCISES});
                const exercise = _data.exercises.filter((exercise: Exercise) => {
                    return (exercise.id === variables.exerciseId)
                })[0];


                const newShoppingCartItem = {
                    id: uniqueId(),
                    __typename: 'ShoppingCartItem',
                    product: {
                        __typename: 'Exercise',
                        id: exercise.id,
                        name: exercise.name,
                        price: exercise.price,
                        image: '/files/' + kebabCase(exercise.name) + '.jpg',
                    }
                };

                const data = {
                    addToShoppingCartItemId: exercise.id,
                    cartDrawerOpen: true,
                    shoppingCartItems: [...previous.shoppingCartItems, newShoppingCartItem]
                };

                cache.writeData({data});
                return data;
            },
            addLuckyCharmToShoppingCart: (_, variables, {cache, getCacheKey}) => {
                const previous = cache.readQuery({query: GET_SHOPPING_CART_ITEMS_QUERY});
                const _data: { luckyCharms: LuckyCharm[] } = cache.readQuery({query: ALL_LUCKY_CHARMS});
                const luckyCharm = _data.luckyCharms.filter((luckyCharm: LuckyCharm) => {
                    return (luckyCharm.id === variables.luckyCharmId)
                })[0];


                const newShoppingCartItem = {
                    id: uniqueId(),
                    __typename: 'ShoppingCartItem',
                    product: {
                        __typename: 'LuckyCharm',
                        id: luckyCharm.id,
                        name: (luckyCharm.workoutType === 'REPETITION' as WORKOUT_TYPES) ?
                            luckyCharm.value + ' sec. minder' + ' oefening uitvoeren'
                            :
                            luckyCharm.value + ' herhalingen verminderen'
                        ,
                        price: luckyCharm.price,
                        image: null
                    }
                };

                const data = {
                    cartDrawerOpen: true,
                    shoppingCartItems: [...previous.shoppingCartItems, newShoppingCartItem]
                };

                cache.writeData({data});
                return data;
            },
            updateNetworkStatus: (_, {isConnected}, {cache}) => {
                const data = {
                    networkStatus: {
                        __typename: 'NetworkStatus',
                        isConnected
                    },
                };
                cache.writeData({data});
                return null
            },
        },
    }
});

const client = new ApolloClient({
    link: ApolloLink.from([
        stateLink,
        new HttpLink({uri: process.env.GRAPHQL_URL || 'http://localhost:4000'})
    ]),
    cache
});

// client.onResetStore(stateLink.writeDefaults as any);


ReactDOM.render(
    <ApolloProvider client={client}>
        <App/>
    </ApolloProvider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
