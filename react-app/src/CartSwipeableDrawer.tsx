import {Theme, WithStyles, withStyles} from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import CloseIcon from '@material-ui/icons/Close';
import CardGiftCardIcon from '@material-ui/icons/CardGiftcard';
import Typography from '@material-ui/core/Typography';
import * as React from "react";
import {
    Avatar,
    Card,
    CardContent,
    Grid,
    IconButton,
    List,
    ListItem,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText,
    SwipeableDrawer,
    Toolbar
} from "@material-ui/core";
import {gql} from "apollo-boost";
import Mutation from "react-apollo/Mutation";
import {chain, map} from 'lodash';
import {compose, graphql} from "react-apollo";

const styles = (theme: Theme) => ({
    drawer: {
        padding: 20,
    },
    list: {
        width: 'auto'
    }
});

const REMOVE_PRODUCTS_FROM_CART_MUTATION = gql`
    mutation removeProductsFromShoppingCart($productId: String!) {
        removeProductsFromShoppingCart(productId: $productId)  @client
    }
`;

export const CLOSE_CART_DRAWER = gql`
    mutation closeCartDrawer {
        closeCartDrawer  @client
    }
`;

export const OPEN_CART_DRAWER = gql`
    mutation openCartDrawer {
        openCartDrawer  @client
    }
`;

export interface IShoppingCartItem {
    id: string,
    product: { id: string, name: string, price: number, image?: string }
}

interface Props extends WithStyles<typeof styles> {
    items: IShoppingCartItem[],
    open: boolean,
    openCartDrawer: Function
    closeCartDrawer: Function
}

export const CartSwipeableDrawer = compose(
    withStyles(styles, {withTheme: true}),
    graphql(CLOSE_CART_DRAWER, {name: 'closeCartDrawer'}),
    graphql(OPEN_CART_DRAWER, {name: 'openCartDrawer'})
)((props: Props) => {
    const {items, open, openCartDrawer, closeCartDrawer} = props;
    return (
        <SwipeableDrawer className={props.classes.drawer} anchor={"right"}
                         open={open}
                         onClose={() => closeCartDrawer() as any} onOpen={() => openCartDrawer() as any}>
            <Toolbar style={{borderBottom: '1px solid #eee'}}>
                <div style={{flexGrow: 1}}><Typography component={"p"} variant={"h5"}>Winkelmand</Typography></div>
                <IconButton onClick={() => closeCartDrawer()} color={"default"}>
                    <CloseIcon/>
                </IconButton>
            </Toolbar>
            {(items.length > 0) ?
                <List className={props.classes.list} dense={false}>
                    {map(chain(items).groupBy('product.id').value(), (groupItems: IShoppingCartItem[], productId: string) => {
                        return (
                            <Mutation mutation={REMOVE_PRODUCTS_FROM_CART_MUTATION}
                                      variables={{productId}}
                                      key={Math.random() * 100000}>
                                {removeProductsFromShoppingCart => (
                                    <ListItem>
                                        <ListItemAvatar>
                                            {(groupItems[0].product.image) ?
                                                <Avatar src={groupItems[0].product.image}/> :
                                                <Avatar><CardGiftCardIcon/></Avatar>}
                                        </ListItemAvatar>
                                        <ListItemText primary={(groupItems.length > 1)
                                            ? groupItems.length + ' x ' + groupItems[0].product.name
                                            : groupItems[0].product.name}
                                                      secondary={'€' + groupItems[0].product.price * groupItems.length}/>
                                        <ListItemSecondaryAction>
                                            <IconButton aria-label="Delete"
                                                        onClick={() => removeProductsFromShoppingCart() as any}>
                                                <DeleteIcon/>
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                )}
                            </Mutation>
                        )
                    })}
                    <Card elevation={0}>
                        <CardContent>
                            <Typography variant={"body2"} component={"p"}>
                                <ul>
                                    <li>
                                        Minimaal 8 oefeningen (1 of meer zelfde oefeningen is toegestaan),
                                    </li>
                                    <li>
                                        maximaal 3 lucky charms (1 of meer zelfde lucky charms is toegestaan)
                                    </li>
                                    <li>
                                        budget is €500,-
                                    </li>
                                </ul>
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card elevation={2}>
                        <CardContent>
                            <Grid container style={{padding: 20}}>
                                <Grid item xs={12}>
                                    <Grid container>
                                        <Typography variant="body1" component="div"
                                                    style={{flexGrow: 1, textAlign: 'right'}}>
                                            Aantal lucky charms: &nbsp;
                                        </Typography>
                                        <Typography variant="body1" component="p">
                                            {items.filter((item: any) => {
                                                return item.product.__typename === 'LuckyCharm';
                                            }).length}
                                        </Typography>
                                    </Grid>
                                    <Grid container>
                                        <Typography variant="body1" component="p"
                                                    style={{flexGrow: 1, textAlign: 'right'}}>
                                            Aantal oefeningen: &nbsp;
                                        </Typography>
                                        <Typography variant="body1" component="p">
                                            {items.filter((item: any) => {
                                                return item.product.__typename === 'Exercise';
                                            }).length}
                                        </Typography>
                                    </Grid>
                                    <Grid container style={{paddingTop: 10}}>
                                        <Typography variant="h5" component="p"
                                                    style={{flexGrow: 1, textAlign: 'right', marginRight: 10}}>
                                            <strong>Totaal bedrag</strong>
                                        </Typography>
                                        <Typography variant="h5" component="p">
                                            <strong>€ {items.reduce((acc: number, cur: IShoppingCartItem) => {
                                                return acc + cur.product.price;
                                            }, 0)}</strong>
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </List>
                :
                <List className={props.classes.list}>
                    <ListItem>
                        <ListItemText primary={"Winkelmand is leeg"}/>
                    </ListItem>
                </List>
            }
        </SwipeableDrawer>
    )
});
