import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {WithStyles, withStyles, WithTheme} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ShopIcon from '@material-ui/icons/Shop';
import ListItemText from '@material-ui/core/ListItemText';
import CartToolbar from "./CartToolbar";
import gql from "graphql-tag";
import {compose, graphql, Query} from "react-apollo";
import {Shop} from "../../prisma-server/generated/prisma-client";
import {ListSubheader, TextField} from "@material-ui/core";
import {NavLink, RouteComponentProps, withRouter} from "react-router-dom";
import Mutation from "react-apollo/Mutation";
import {GET_SHOPPING_CODE} from "./ExerciseCard";
import {Theme} from "@material-ui/core/es";

export const GET_ALL_SHOPS = gql`
    {
        shops {
            id
            name
            shoppingCode
        }
    }
`;

export const SET_SHOPPING_CODE = gql`
    mutation  setShoppingCode($shoppingCode: String!) {
        setShoppingCode(shoppingCode: $shoppingCode) @client
    }
`;
const drawerWidth = 240;

const styles = theme => ({
    yourShop: {
        color: theme.palette.primary.main
    },
    textField: {
        // marginLeft: theme.spacing.unit,
        // marginRight: theme.spacing.unit,
        // width: 200,
    },
    root: {
        display: 'flex',
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        })
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: '0 8px',
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        paddingLeft: theme.spacing.unit * 2,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0
    },
    active: {
        backgroundColor: theme.palette.action.selected
    },
    menuIcon: {
        marginLeft: 0,
        paddingLeft: 0
    }
});

interface Props extends WithStyles<typeof styles>, RouteComponentProps, WithTheme {
    getShoppingCode: any
}

class PersistentDrawerLeft extends React.Component<Props, any> {
    state = {
        open: false,
        shoppingCode: ''
    };
    textInput;

    handleDrawerOpen = () => {
        this.setState({open: true});
    };

    handleDrawerClose = () => {
        this.setState({open: false});
    };

    componentDidMount(): void {
        this.setState({open: true});
        this.textInput.focus();
    }

    render() {
        const {classes, theme} = this.props;
        const {open} = this.state;

        return (
            <div className={classes.root}>
                <CssBaseline/>
                <AppBar
                    position="fixed"
                    className={classNames(classes.appBar, {
                        [classes.appBarShift]: open,
                    })}
                >
                    <Toolbar className={classNames(!open && classes.menuIcon)}>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerOpen}
                            className={classNames(classes.menuButton, open && classes.hide)}
                        >
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="h6" color="inherit" noWrap style={{flexGrow: 1}}>
                            Exercises
                        </Typography>
                        <CartToolbar />
                    </Toolbar>
                </AppBar>
                <Drawer
                    className={classes.drawer}
                    variant="persistent"
                    anchor="left"
                    open={open}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.drawerHeader}>
                        <IconButton onClick={this.handleDrawerClose}>
                        {theme.direction === 'ltr' ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                        </IconButton>
                    </div>
                    <Divider/>
                    <List subheader={<ListSubheader component="div">Winkels</ListSubheader>}>
                        <Query query={GET_ALL_SHOPS}>
                            {({loading, error, data}) => {
                                if (loading) return 'Loading...';
                                if (error) return `Error! ${error.message}`;
                                return (
                                    <React.Fragment>
                                        {data.shops.map((shop: Shop) => {
                                            const yourShopStyle = (this.props.getShoppingCode.shoppingCode === shop.shoppingCode) ? classes.yourShop : "";
                                            return (
                                                <ListItem {...{
                                                    component: NavLink,
                                                    to: `/shops/${shop.id}`,
                                                    activeClassName: classes.active
                                                } as any} button key={shop.id + Math.random() * 10000}>
                                                    <ListItemIcon
                                                        classes={{root: yourShopStyle}}><ShopIcon/></ListItemIcon>
                                                    <ListItemText
                                                        classes={{primary: yourShopStyle, secondary: yourShopStyle}}
                                                        primary={shop.name}
                                                        secondary={(this.props.getShoppingCode.shoppingCode === shop.shoppingCode) ? 'Jouw shop' : ''}/>
                                                </ListItem>
                                            )
                                        })}
                                    </React.Fragment>
                                )
                            }}
                        </Query>
                    </List>
                    <Divider/>
                    <List subheader={<ListSubheader component="div">Extra's</ListSubheader>}>
                        <ListItem {...{
                            component: NavLink,
                            to: `/lucky-charms`,
                            activeClassName: classes.active
                        } as any} button>
                            <ListItemIcon><CardGiftcardIcon/></ListItemIcon>
                            <ListItemText primary={"Lucky Charms"}/>
                        </ListItem>
                    </List>
                    <List>
                        <ListItem>
                            <Mutation mutation={SET_SHOPPING_CODE}
                                      variables={{shoppingCode: this.state.shoppingCode as any}}>
                                {setShoppingCode => {
                                    return (
                                        <Query query={GET_SHOPPING_CODE}>
                                            {({loading, error, data}) => {
                                                if (loading) return 'Loading...';
                                                if (error) return `Error! ${error.message}`;
                                                return (
                                                    <TextField
                                                        inputRef={(input) => { this.textInput = input; }}
                                                        autoFocus={true}
                                                        placeholder={"Voer hier code in"}
                                                        id="standard-name"
                                                        label="Shopping code"
                                                        className={classes.textField}
                                                        value={data.shoppingCode}
                                                        inputProps={{autoFocus: true}}
                                                        onChange={(e: any) => {
                                                            this.setState({shoppingCode: e.target.value}, () => {
                                                                setShoppingCode();
                                                            });

                                                        }}
                                                    />
                                                )
                                            }}
                                        </Query>
                                    )
                                }}
                            </Mutation>
                        </ListItem>
                    </List>
                </Drawer>
                <main
                    className={classNames(classes.content, {
                        [classes.contentShift]: open,
                    })}
                >
                    {this.props.children}
                </main>
            </div>
        );
    }
}

PersistentDrawerLeft['propTypes'] = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default compose(
    withRouter,
    withStyles(styles, {withTheme: true}),
    graphql(GET_SHOPPING_CODE, {name: 'getShoppingCode'})
)(PersistentDrawerLeft);
