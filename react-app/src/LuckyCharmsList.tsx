import gql from 'graphql-tag';
import {Query} from 'react-apollo';
import React from 'react';
import * as _ from 'lodash';
import {Grid} from "@material-ui/core";
import LuckyCharmCard from "./LuckyCharmCard";
import {LuckyCharm} from "../../prisma-server/generated/prisma-client";

export const ALL_LUCKY_CHARMS = gql`
    {
        luckyCharms {
            id
            workoutType
            operation
            value
            price
        }
    }
`;


export const LuckyCharmsList = (props: any) => (
    <React.Fragment>
        <Query query={ALL_LUCKY_CHARMS}>
            {({loading, error, data}) => {
                if (loading) return 'Loading...';
                if (error) return `Error! ${error.message}`;
                return (
                    <Grid container spacing={24}>
                        {data.luckyCharms.map((luckyCharm: LuckyCharm) => {
                            return (
                                <Grid item xs={12} md={4} key={luckyCharm.id}>
                                    <Grid container justify={"center"}>
                                        <LuckyCharmCard luckyCharm={luckyCharm}/>
                                    </Grid>
                                </Grid>
                            )
                        })}
                    </Grid>
                );
            }}
        </Query>
    </React.Fragment>
);