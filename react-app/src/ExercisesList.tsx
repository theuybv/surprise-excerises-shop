import gql from 'graphql-tag';
import {Query} from 'react-apollo';
import React from 'react';
import * as _ from 'lodash';
import {Exercise} from "../../prisma-server/generated/prisma-client";
import ExerciseCard from "./ExerciseCard";
import {Grid} from "@material-ui/core";
import {Redirect} from "react-router";

export const GET_EXERCISES = gql`
    {
        exercises {
            id
            name
            videoId
            price
            thumbnailId
        }
    }
`;

export const GET_EXERCISES_FROM_SHOP_ID = gql`
    query getExercisesFromShopId($shopId: ID!) {
        shops(where:{id:$shopId}) {
            id
            name
            exercises {
                id
                name
                videoId
                price
                thumbnailId
                shop {
                    shoppingCode
                }
            }
        }
    }
`;


export const ExercisesList = ({shopId}: { shopId: string }) => (
    <React.Fragment>
        <Query query={GET_EXERCISES}>
            {({loading, error, data}) => <React.Fragment></React.Fragment>}
        </Query>
        <Query query={GET_EXERCISES_FROM_SHOP_ID} variables={{shopId}}>
            {({loading, error, data}) => {
                if (loading) return 'Loading...';
                if (error) return <Redirect to={'/'} push={true}/>;
                const videoSrcTag = (exercise: Exercise) => {
                    return (
                        <video width="320" poster={'files/' + _.kebabCase(exercise.name) + '.jpg'} controls
                               autoPlay={false}
                               preload={'none'}
                               key={Math.random() * 10000}>
                            <source src={'files/' + _.kebabCase(exercise.name) + '.mp4'} type="video/mp4"/>
                            Your browser does not support the video tag.
                        </video>
                    )
                };
                return (
                    <Grid container spacing={24}>
                        {data.shops[0].exercises.map((exercise: Exercise) => {
                            return (
                                <Grid item xs={12} md={4} key={exercise.id}>
                                    <Grid container justify={"center"}>
                                        <ExerciseCard
                                            exercise={exercise}
                                            videoSrcTag={videoSrcTag(exercise)}
                                        />
                                    </Grid>
                                </Grid>
                            )
                        })}
                    </Grid>
                );
            }}
        </Query>
    </React.Fragment>
);